package com.example.myapplicamk

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.CallLog
import android.provider.ContactsContract
import android.widget.Button
import android.widget.EditText
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    private lateinit var editTextEmail: EditText
    private lateinit var editTextPassword: EditText
    private lateinit var buttonLogin: Button
    private lateinit var buttonRegister: Button
    private lateinit var buttonRestPassword: Button





    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        init()

        registerListeners()
    }

    private fun init() {
        editTextEmail = findViewById(R.id.editTextEmail)
        editTextPassword = findViewById(R.id.editTextPassword)
        buttonLogin = findViewById(R.id.buttonLogin)
        buttonRegister = findViewById(R.id.buttonRegister)
        buttonRestPassword = findViewById(R.id.buttonRestPassword)
        var password2:EditText = findViewById(R.id.editTextRepeatPassword)

    }
    private fun registerListeners() {
        buttonRegister.setOnClickListener {
            startActivity(Intent(this, RegisterActivity::class.java))
        }
        buttonRestPassword.setOnClickListener {
                startActivity(Intent(this, PasswordResetActivity::class.java))
        }
        buttonLogin.setOnClickListener{
            val email =editTextEmail.text.toString()
            val password = editTextPassword.text.toString()


            if(  !email.isEmpty() && email.contains("@") && !password.isEmpty()  &&  password.toString().equals(password.toString())){

                FirebaseAuth.
                getInstance().
                createUserWithEmailAndPassword(email.toString(),password.toString()).
                addOnCompleteListener {  task ->
                    if(task.isSuccessful) {
                        Toast.makeText(this,"succesfuly registered !",Toast.LENGTH_SHORT).show()  }
                    else{ Toast.makeText(this,"Error   !",Toast.LENGTH_SHORT).show()     } }
            }


        }

    }
}